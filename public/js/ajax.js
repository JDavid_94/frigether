function mostrar(e) {
    var t = document.getElementById("cajaStatus"), n = document.getElementById("status");
    e ? (t.className = "green lighten-5", t.style.borderColor = "#c8e6c9", n.className = "green-text text-darken-4") : (t.className = "red lighten-5", t.style.borderColor = "#ef9a9a", n.className = "red-text text-darken-4"), t.style.display = "block"
}

function mostrarJugador() {
    document.getElementById("jugador").style.display = "block"
}

function mostrarTabla() {
    document.getElementById("tabla").style.display = "block"
}

function reconocer(e) {
    var t = !0;
    return "!%" == e.substr(0, 2) && (t = !1), t
}

function reconocerJson(e) {
    var t = !0;
    return "error" == e ? t = !1 : document.getElementById("todos").click(), t
}

function recortar(e, t) {
    return e || (t = t.substr(2, t.length)), t
}

function conexionJson(e, t) {
    var n = document.getElementsByClassName("form-control"), o = new XMLHttpRequest;
    o.open("post", "Controlador", !0), o.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), o.onreadystatechange = function () {
        if (4 == this.readyState && 200 == this.status) {
            var e = JSON.parse(this.responseText);
            "resultado" == t ? reconocerJson(e.tipo) ? (mostrarJugador(), document.getElementById(t).innerHTML = e.texto) : (mostrar(reconocerJson(e.tipo)), document.getElementById("status").innerHTML = e.texto) : "status" == t ? (mostrar(reconocerJson(e.tipo)), document.getElementById("status").innerHTML = e.texto) : (mostrarTabla(), document.getElementById(t).innerHTML = e.texto)
        }
    }, o.send(encodeURI(e + "=" + e + "&idJugador=" + n[0].value + "&nickName=" + n[1].value + "&nombreJugador=" + n[2].value + "&correoJugador=" + n[3].value))
}

window.onload = function () {
    document.getElementById("agregar").onclick = function () {
        return document.getElementById("cajaStatus").style.display = "none", conexionJson("agregar", "status"), !1
    }, document.getElementById("eliminar").onclick = function () {
        return document.getElementById("cajaStatus").style.display = "none", conexionJson("eliminar", "status"), !1
    }, document.getElementById("buscar").onclick = function () {
        return document.getElementById("cajaStatus").style.display = "none", conexionJson("buscar", "resultado"), !1
    }, document.getElementById("todos").onclick = function () {
        return document.getElementById("cajaStatus").style.display = "none", conexionJson("todos", "contenidoTabla"), !1
    }, document.getElementById("modificar").onclick = function () {
        return document.getElementById("cajaStatus").style.display = "none", conexionJson("modificar", "status"), !1
    }, document.getElementById("rellenar").onclick = function () {
        document.getElementById("cajaStatus").style.display = "none";
        for (var e = document.getElementById("resultado"), t = document.getElementsByTagName("form")[0], n = e.getElementsByTagName("p"), o = t.getElementsByTagName("input"), a = 1; a < n.length; a++) o[a].value = n[a].innerHTML;
        return !1
    }
};