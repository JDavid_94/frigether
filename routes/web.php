<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','GuestController@inicio');

/* Rutas de Invitados*/

Auth::routes();

Route::get('/login/admin', 'LoginAdministratorController@showLoginForm');
Route::post('/login/admin', 'LoginAdministratorController@login');

/*Rutas de Usuarios registrados*/
Route::get('/inicio','GeneralController@inicio');
Route::get('/category', 'GeneralController@select');
//Route::get('/home', 'GeneralController@index');
Route::get('/evento','EventoController@show');
Route::post('/evento','EventoController@create');
Route::get('/evento/{categoria?}','EventoController@select');

/*Rutas de Administradores*/
Route::prefix('admins')->group(function (){
    Route::get('/inicio','BackendController@inicio');
  //  Route::get('/area', 'BackendController@show');
    Route::get('/category', 'BackendController@select');
    Route::get('/create-category', 'BackendController@createShow');
    Route::get('/edit/{categoria?}', 'BackendController@edit');
    Route::post('/edit/{categoria?}', 'BackendController@update');
    Route::post('/add', 'BackendController@add');
    Route::post('/remove', 'BackendController@delete');
});

