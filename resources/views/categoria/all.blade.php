@extends('master')
@section('title', 'Categorias')
@section('script')
    <script type="text/javascript" src="/js/eliminar-categorias.js"></script>
@endsection
@section('content')

    @auth('admins')
        <h2 class="heading text-center espacio-top">Bontones de administración</h2>
        <div class="row espacio-bot">
            <div class=" col-md-6">
                <a href="/admins/create-category" class="btn btn-success btn-block"><i
                            class="fa fa-caret-square-o-up"></i> Agregar Categoria</a>
            </div>


            <form method="POST" action="/admins/remove" class=" col-md-6">
                @csrf
                <button type="submit" class="btn btn-red btn-block"><i class="fa fa-close"></i> Eliminar Categoria
                </button>
                <div id="insertar">
                </div>
            </form>
        </div>
    @endauth
    <div class="row">

        @foreach($categorias as $categoria)
            <div class=" col-md-4">

                <div class="card card-image tarjeta"
                     style="background-image: url({{Storage::url($categoria->imagen_categoria)}})">

                    <!-- Content -->
                    <div class="text-azul text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                        <div>

                            <h5 class="white-text"> {{$categoria->nombre_categoria}}</h5>
                            @auth('web')
                                <a href="/evento/{{$categoria->id_categoria}}" class="btn btn-red"><i
                                            class="fa fa-hand-pointer-o left"></i> Acceder</a>
                            @endauth
                            @auth('admins')

                                <a class="btn btn-warning"
                                   href="{!! action('BackendController@edit', $categoria->nombre_categoria) !!}"><i
                                            class="fa fa-edit"></i></i> Editar Categoria</a>

                                <div class="coloured">
                                    <div class="checkbox">
                                        <label class="text-white">
                                            <input type="checkbox" name="categoria"
                                                   value="{{$categoria->id_categoria}}">

                                            <span class="checkbox-material"><span
                                                        class="check text-white"></span></span>

                                        </label>
                                    </div>
                                </div>
                            @endauth
                        </div>
                    </div>
                    <!-- Content -->
                </div>
            </div>
        @endforeach

    </div>


@endsection