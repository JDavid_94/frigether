@extends('master')
@section('title', 'Eventos')
@section('script')

@endsection
@section('content')
    <div class="row">
        @foreach($eventos as $evento)
            <div class=" offset-2 col-md-8">
                <div class="card">
                    <div class="card-header primary-color white-text"><h3>{{$evento->nombre_evento}}</h3>

                    </div>
                    <div class="card-body">
                        <p class="card-title">{{$evento->comunidad_evento}}, {{$evento->municipio_evento}} el
                            @php
                                $date = new DateTime($evento->fecha_evento);
                                echo $date->format('d-m-Y')
                            @endphp.</p>
                        <p class="card-text">{{$evento->descripcion_evento}}</p>
                        <a class="btn btn-primary disabled">Apuntarse</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection