@extends('master')
@section('title', 'Add category')
@section('script')

@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Crear Evento</div>

                <div class="card-body">
                    <form method="POST">
                        @csrf

                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Nombre del evento</label>

                            <div class="col-md-12">
                                <input type="text" name="nombre_evento" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Categoria</label>

                            <select name="id_categoria" class="offset-1 col-md-3">
                                @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Comunidad autonoma</label>

                            <div class="col-md-12">
                                <input type="text" name="comunidad_evento" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Municipio</label>

                            <div class="col-md-12">
                                <input type="text" name="municipio_evento" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Fecha del evento</label>

                            <div class="col-md-12">
                                <input type="date" name="fecha_evento" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label text-md-left">Descripcion del evento</label>

                            <div class="col-md-12">
                                <textarea name="descripcion_evento" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-azul  btn-block">
                                    Crear
                                </button>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection