@extends('master')
@section('title', 'Inicio')
@section('content')
    <!--Carousel Wrapper-->
    <div class="row">
        <div id="carousel-example-1z" class="col-md-12 carousel slide carousel-fade" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-1z" data-slide-to="1"></li>

            </ol>
            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <!--First slide-->
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/img/slide/snow.jpeg"
                         alt="First slide">
                </div>
                <!--/First slide-->
                <!--Second slide-->
                <div class="carousel-item">
                    <img class="d-block w-100" src="/img/slide/escalada.jpeg"
                         alt="Second slide">
                </div>
                <!--/Second slide-->

            </div>
            <!--/.Slides-->
            <!--Controls-->
            <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!--/.Controls-->
        </div>
        <!--/.Carousel Wrapper-->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading col-md-12">
                    <h1 class="azul-text text-center espacio-top">¿Buscas gente con tus aficiones?</h1>
                </div>
                <div class="panel-content">
                    <p class="text-center">Estas de suerte aqui encontraras gente que busca hacer cosas pero no tiene
                        con quien hacerlas o
                        simplemente prefiere hacerlas en compañia de otras personas</p>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class=" card card-image" id="deporte">

                            <!-- Content -->
                            <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                                <div>
                                    <h5 class="red-text"><i class="fa fa-soccer-ball-o" aria-hidden="true"></i> Deporte</h5>
                                    <h3 class="card-title pt-2"><strong>Salir a correr</strong></h3>
                                    <p>Quieres planear una ruta y proponerla a otros Runners de la zona de esta manera
                                        haras un reto solo de hacer tu deporte diario.</p>
                                    <a class="btn btn-red"><i class="fa fa-hand-pointer-o left"></i> Acceder</a>
                                </div>
                            </div>
                            <!-- Content -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class=" card card-image" id="clases">

                            <!-- Content -->
                            <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                                <div>
                                    <h5 class="text-azul"><i class="fa fa-book" aria-hidden="true"></i> Clases</h5>
                                    <h3 class="card-title pt-2"><strong>Estudios</strong></h3>
                                    <p>Te gustaria dar clases de una materia a cambio de otra así aprendereis juntos diferentes materias, es una gran manera de aprender y gratuita.</p>
                                    <a class="btn btn-azul"><i class="fa fa-hand-pointer-o left"></i> Acceder</a>
                                </div>
                            </div>
                            <!-- Content -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class=" card card-image" id="baile">

                            <!-- Content -->
                            <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                                <div>
                                    <h5 class="orange-text"><i class="fa  fa-hand-peace-o" aria-hidden="true"></i> Danza</h5>
                                    <h3 class="card-title pt-2"><strong>Break dance</strong></h3>
                                    <p>Montar un grupo de Break dance es tu objetivo, aqui podras proponer donde van a ser los entranmientos y buscar a gente que comparta tu afición.</p>
                                    <a class="btn btn-orange"><i class="fa fa-hand-pointer-o left"></i> Acceder</a>
                                </div>
                            </div>
                            <!-- Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection