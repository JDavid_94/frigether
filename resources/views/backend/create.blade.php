@extends('master')
@section('title', 'Add category')
@section('script')

@endsection
@section('content')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Añadir categoria</div>

                    <div class="card-body">
                        <form method="POST" action="/admins/add" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-12 col-form-label text-md-left">Nombre de la categoria</label>

                                <div class="col-md-12">
                                    <input type="text" name="nombre" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-12 col-form-label text-md-left">Imagen de fondo</label>

                                <div class="col-md-12">

                                    <input type="file" name="file">


                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-md-12 ">
                                    <button type="submit" class="btn btn-azul btn-block">
                                        Crear
                                    </button>


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection