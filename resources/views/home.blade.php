@extends('master')

@section('content')

    <div class="row">
        <div class="col-md-2 offset-1">
            <div class="panel">
                <div class="panel-heading">
            <img id="img-perfil"
                 src="https://vignette.wikia.nocookie.net/lossimpson/images/a/ab/BartSimpson.jpg/revision/latest?cb=20120201020952&path-prefix=es"
                 alt="">
                    <h3>Eventos </h3>
                </div>
                <div class="panel-content">
                    <ul>
                        <li><a href="#">Running</a></li>
                        <li><a href="#">Snow</a></li>
                        <li><a href="#">Biblioteca</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card  bs-component">
                <form class="form-horizontal" method="post">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach

                    @if (session('status'))
                        <div class = "alert alert-success">
                            {{ session ('status') }}
                        </div>
                    @endif

                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <fieldset>
                        <div class="form-group ">
                            <label for="imagen" class="col-md-12 control-label">Categoria</label>
                            <div class="col-lg-4">
                                <select id="selected" name="selected" class="form-control">
                                    @foreach($categorias as $categoria)
                                    <option>{{$categoria->nombre_categoria}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <legend class="text-center">Crear Evento</legend>
                        <div class="form-group">
                            <label for="nombre" class="col-lg-12 control-label">Nombre</label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="nombre" placeholder="nombre del producto" name="nombre">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="precio" class="col-lg-12 control-label">Precio</label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="precio" placeholder="20.00" name="precio">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imagen" class="col-lg-12 control-label">Nombre de la imagen</label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="imagen" placeholder="hamburguesa.jpg" name="imagen">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="descripcion" class="col-lg-12 control-label">descripcion</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion"></textarea>
                                <span class="help-block">Incluye toda la descripcion del producto.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12 col-lg-offset-2">
                                <button class="btn btn-default">Cancelar</button>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
