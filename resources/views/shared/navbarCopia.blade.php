<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar" id="barra-scroll">
    @auth('admins')
        <a class="navbar-brand" href=" {{ url('/admins/incio') }} ">FriGether</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3"
                aria-controls="navbarSupportedContent-3"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-3">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/home') }}">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/admins/category')}}">Categorias</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:;;">Pricing</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-3" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Dropdown
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-unique"
                         aria-labelledby="navbarDropdownMenuLink-3">
                        <a class="dropdown-item " href="javascript:;;">Action</a>
                        <a class="dropdown-item  white-text" href="javascript:;;">Another action</a>
                        <a class="dropdown-item  white-text" href="javascript:;;">Something else here</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"><i class="fa fa-user "> <span
                                    class="h5-responsive">{{ Auth::user()->name }}</span></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right dropdown-unique"
                         aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>

                    </div>
                </li>
            </ul>
            @endauth
            @auth('web')
                <a class="navbar-brand" href=" {{ url('/inicio') }} ">FriGether</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent-3"
                        aria-controls="navbarSupportedContent-3"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/home') }}">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/category')}}">Categorias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:;;">Pricing</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-3" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">Dropdown
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-unique"
                                 aria-labelledby="navbarDropdownMenuLink-3">
                                <a class="dropdown-item " href="javascript:;;">Action</a>
                                <a class="dropdown-item  white-text" href="javascript:;;">Another action</a>
                                <a class="dropdown-item  white-text" href="javascript:;;">Something else here</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"><i class="fa fa-user "> <span
                                            class="h5-responsive">{{ Auth::user()->name }}</span></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right dropdown-unique"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </li>
                    </ul>
                    @endauth
                    @guest
                        <a class="navbar-brand" href=" {{ url('/') }} ">FriGether</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent-3"
                                aria-controls="navbarSupportedContent-3"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
                            <ul class="navbar-nav ml-auto nav-flex-icons">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                                </li>
                            </ul>
                            @endguest
                        </div>
</nav>