<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function( Blueprint $table){
            $table->increments('id_evento')->unique();
            $table->integer('id_categoria');
            $table->string('nombre_evento');
            $table->string('comunidad_evento');
            $table->string('municipio_evento');
            $table->date('fecha_evento');
            $table->string('descripcion_evento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
