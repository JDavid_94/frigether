<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $guarded = ['id_evento'];

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
}
