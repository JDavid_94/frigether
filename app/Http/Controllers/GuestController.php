<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GuestController extends Controller
{
    function inicio()
    {
        if (Auth::guard('admins')->check()) {
            return redirect('/admins/inicio');
        }
        if (Auth::guard('web')->check()) {
            return redirect('/inicio');
        }
        return view('welcome');
    }
}
