<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginAdministratorController extends Controller
{
    use AuthenticatesUsers;


    function redirectTo()
    {
        return '/admins/inicio';

    }

    function guard()
    {
        return Auth::guard('admins');
    }

//   protected $guard = 'admins';

    function showLoginForm()
    {
        if (Auth::guard('admins')->check()) {
            return redirect('/admins/inicio');
        }
        if (Auth::guard('web')->check()) {
            return redirect('/inicio');
        }
        return view('auth.ad');
    }


}
