<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    function inicio()
    {
        return view('welcome');
    }

    function show()
    {
        return view('backend.home');
    }

    function select()
    {
        $categorias = Categoria::all();
        return view('categoria.all', compact('categorias'));
    }

    function createShow()
    {
        return view('backend.create');
    }

    function add(Request $request)
    {
        $file = $request->file('file');
        $nombreFile = str_replace(" ", "_", $request->get('nombre'));
        $categoria = new Categoria(array(
            'nombre_categoria' => $request->get('nombre'),
            'imagen_categoria' => $nombreFile . '/' . $file->hashName()
        ));
        Storage::disk('public')->put($nombreFile, $file);
        $categoria->save();
        //dd($file);
        $categorias = Categoria::all();
        return view('categoria.all', compact('categorias'));
    }

    function delete(Request $request)
    {
        //  echo $request->get('categoria');
        $nombres = $request->get("categoria");
        //$imagenes = $_POST["imagen"];

        for ($i = 0; $i < count($nombres); $i++) {
            // echo "<br> Provincias " . $i . ": " . $categoria[$i];


            $nombre = $nombres[$i];
            $categoria = Categoria::whereId_categoria($nombre);
            $categoria->delete();

            Storage::disk('public')->deleteDirectory($nombres[$i]);


        }
        $categorias = Categoria::all();
        return view('categoria.all', compact('categorias'));
    }

    function edit($categoria)
    {
        $categoria = Categoria::whereNombre_categoria($categoria)->firstOrFail();
        return view('backend.edit', compact('categoria'));
    }

    function update(Request $request)
    {

        $file = $request->file('file');
        $nombreFile = str_replace(" ", "_", $request->get('nombre'));
        $nombre = $request->get('nombre');
        $nombreAntiguo = $request->get('nombreAntiguo');
        $id = $request->get('id');
        echo $file . ' ';
        echo $nombreFile . ' ';
        echo $nombre . ' ';
        echo $nombreAntiguo . ' ';
        echo $id;

        Storage::disk('public')->deleteDirectory($nombreAntiguo);
        /*
                $categoria = Categoria::whereId_categoria($id);
                $categoria->nombre_categoria=$nombre;
                $categoria->imagen_categoria=$nombreFile;
                $categoria->save();
               // print_r($categoria);
        */
        Categoria::whereId_categoria($id)->update([
            // 'id'=> $id,
            'nombre_categoria' => $nombre,
            'imagen_categoria' => $nombreFile . '/' . $file->hashName(),
        ]);


        Storage::disk('public')->put($nombreFile, $file);
        // $categoria= $request->get('nombre');
        return redirect('/admins/category');


    }


}
