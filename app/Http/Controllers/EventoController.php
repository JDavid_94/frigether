<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Evento;
use Illuminate\Http\Request;

class EventoController extends Controller
{
    function show(){
        $categorias = Categoria::all();
        return view('evento.evento', compact('categorias'));
    }
    public function create (Request $request)
    {
        $evento = new Evento(array(
            'id_categoria' => $request->get('id_categoria'),
            'nombre_evento' => $request->get('nombre_evento'),
            'comunidad_evento' => $request->get('comunidad_evento'),
            'municipio_evento' => $request->get('municipio_evento'),
            'fecha_evento' => $request->get('fecha_evento'),
            'descripcion_evento' => $request->get('descripcion_evento'),

        ));

        $evento->save();

        return redirect()->back();
    }
    function select($idCategoria){
        $eventos = Evento::whereId_categoria($idCategoria)->get();
        return view('evento.all', compact('eventos'));
    }
}
