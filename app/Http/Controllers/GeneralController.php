<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

class GeneralController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }
    function inicio(){
        return view('welcome');
    }

    public function index()
    {
        $categorias = Categoria::all();
        return view('home', compact('categorias'));
    }
    function select(){
       $categorias=Categoria::all();
       return view('categoria.all',compact('categorias'));
   }
}
