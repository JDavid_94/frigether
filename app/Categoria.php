<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Categoria extends Authenticatable
{

    protected $fillable = [
        'nombre_categoria', 'imagen_categoria',
    ];
    public function evento()
    {
        return $this->hasMany('App\evento', 'id_categoria');
    }
}
